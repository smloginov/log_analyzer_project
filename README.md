# LOG ANALYZER

Log-analyzer -- command line tool for analyzing Nginx log files. 


## Installing

```python
cd containing_folder/
python setup.py install
```
## Usage

```
log_analyzer options

Options:

--config -- path to config file
--bad_parse_thresh -- threshold for bad parse detection
--logging_file_path -- path to logging file
```

## Running the tests
In folder, containing project, run:

`python -m unittest develop`


## License

This project is licensed under the MIT License.

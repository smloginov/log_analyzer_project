#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os
import datetime
import re
import json
import gzip
import logging
import tempfile
import shutil
from collections import defaultdict, namedtuple
from string import Template


LOG_FORMAT_UI_SHORT = '$remote_addr $remote_user $http_x_real_ip [$time_local] ' \
                      '"$request" $status $body_bytes_sent "$http_referer" ' \
                      '"$http_user_agent" "$http_x_forwarded_for" ' \
                      '"$http_X_REQUEST_ID" "$http_X_RB_USER" $request_time'

config = {
    "REPORT_SIZE": 5,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "/home/serg/python_course/01_advanced_basics/homework/log"
}


def median(data):
    n = len(data)
    return data[n // 2] if n % 2 == 1 \
        else (data[n // 2 - 1] + data[n // 2]) / 2


def setup_logger(logger_path):

    logging.basicConfig(level=logging.INFO,
                        filename=logger_path,
                        format='%(asctime)s %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')


def parse_args():
    
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--config',
                        action='store_const',
                        const='configs/config.json',
                        help='path to config file')

    parser.add_argument('--bad_parse_thresh', type=float,
                        default=0.5,
                        help='threshold for bad parse detection')

    parser.add_argument('--logging_file_path', type=str,
                        help='logging file path')

    return parser.parse_args()



def build_regex_pattern():

    specific_patterns = {
        'request_time': '\d*\.\d+|\d+',
    }

    regex = ''.join(
        '(?P<%s>%s)' % (g, specific_patterns.get(g, '.*?')) if g
            else re.escape(c)
        for g, c in re.findall(r'\$(\w+)|(.)', LOG_FORMAT_UI_SHORT)
    )
    return re.compile(regex)


def lazy_read_log_file(log_fn, ext):
    open_file = gzip.open if ext == '.gz' else open
    with open_file(log_fn) as f:
        for line in f:
            line = line.decode('utf8')
            yield line


def parse_log_file(lines, pattern):
    return (pattern.match(l) for l in lines)


def calc_stats(matches, report_size):
    count = defaultdict(int)
    time_stat = defaultdict(list)

    all_requests_time = 0.0
    bad_match_counter = 0
    all_records_counter = 0

    for match in matches:
        if match is None:
            bad_match_counter += 1
        else:
            m_dict = match.groupdict()
            url = ' '.join(m_dict['request'].split(' ')[1:-1])

            count[url] += 1

            request_time = float(m_dict["request_time"])
            time_stat[url].append(request_time)
            all_requests_time += request_time

        all_records_counter += 1

    sorted_time_stats = sorted(time_stat.iteritems(),
                               key=lambda (url, time_req_list): sum(
                                   time_req_list),
                               reverse=True)[:report_size]

    json_list = []

    for (url, times_list) in sorted_time_stats:
        times_list = sorted(times_list)
        time_sum = sum(times_list)

        d = {}
        d["count"] = count[url]
        d["time_avg"] = time_sum / max(len(times_list), 1)
        d["time_max"] = times_list[-1]
        d["time_sum"] = time_sum
        d["url"] = url
        d["time_med"] = median(times_list)
        d["time_perc"] = (time_sum / all_requests_time) * 100
        d["count_perc"] = (float(count[url]) / all_records_counter) * 100

        json_list.append(d)

    return (float(bad_match_counter) / all_records_counter,
            json.dumps(json_list))


def find_log_file(log_dir):

    freshLogInfo = namedtuple(
        'freshLogInfo', ['date', 'name', 'ext'])

    pattern = re.compile(
        r'^nginx-access-ui\.log-(?P<date>\d{8})(?P<ext>\.gz)?$')
    today = datetime.datetime.today()
    fresh_log_date = today
    fresh_log_name = ''
    fresh_log_ext = ''
    min_diff_between_dates = float('Inf')

    if os.path.isdir(log_dir):
        for fn in os.listdir(log_dir):
            match = pattern.match(fn)
            if match:
                parsed_log_date = datetime.datetime.strptime(
                    match.group('date'), "%Y%m%d")
                cur_diff_between_dates = (today - parsed_log_date).total_seconds()
                if cur_diff_between_dates < min_diff_between_dates:
                    fresh_log_name = fn
                    fresh_log_date = parsed_log_date
                    fresh_log_ext = match.group('ext')
                    min_diff_between_dates = cur_diff_between_dates

    else:
        logging.error('log dir %s does not exist' % log_dir)
        raise IOError

    return freshLogInfo(date=fresh_log_date,
                        name=fresh_log_name,
                        ext=fresh_log_ext)


def merge_config(default_config, config_from_file_path):
    if not os.path.isfile(config_from_file_path):
        raise IOError("invalid config path")

    with open(config_from_file_path) as f:
        config_from_file = json.load(f, encoding='utf8')
        default_config.update(config_from_file)

    return default_config


def render_report(json_stat, report_path, report_date):

    if not os.path.isdir(report_path):
        os.makedirs(report_path)    

    with open(os.path.join(report_path, 'report.html')) as template_report:
        template = Template(template_report.read().decode('utf8'))
        template_str = template.safe_substitute(table_json=json_stat)
        rendered_report_fn = ''.join(
            ['report-', report_date.strftime('%Y.%m.%d'), '.html'])
        
        with tempfile.NamedTemporaryFile() as rendered_report:
            template_str = template_str.encode('utf8')
            rendered_report.write(template_str)

            shutil.copy(rendered_report.name, os.path.join(
                report_path, rendered_report_fn))

def main():

    logging.info('script starts working')
    logging.info('config file path: %s' % config_from_file_path)
    logging.info('bad parsing threshold: %f' % bad_parse_thresh)
    logging.info('logging file path: %s' % logging_file_path)

    logging.info('result config: %s' % merged_config)
    try:
        fresh_log_info = find_log_file(merged_config["LOG_DIR"])
    except IOError:
        sys.exit(1)

    if not fresh_log_info.name:
        logging.info('no new log files in log folder, stop execution')
        sys.exit(0)

    data_str = fresh_log_info.date.strftime("%Y.%m.%d")
    if os.path.isdir(merged_config["REPORT_DIR"]):
        for report_fn in os.listdir(merged_config["REPORT_DIR"]):
            if data_str in report_fn:
                logging.info('work already done, stop execution')
                sys.exit(0)

    logging.info('new log file %s was detected' % fresh_log_info.name)
    lines = lazy_read_log_file(os.path.join(
        merged_config["LOG_DIR"], fresh_log_info.name), fresh_log_info.ext)

    pattern = build_regex_pattern()
    matches = parse_log_file(lines, pattern)

    bad_parse_ratio, json_stat = calc_stats(
        matches, merged_config["REPORT_SIZE"])
    if bad_parse_ratio > bad_parse_thresh:
        logging.error(
            "exceeding bad parse threshold: (%f > %f)."
            "stop execution"
            % (bad_parse_ratio, bad_parse_thresh))
        sys.exit(1)

    render_report(json_stat, merged_config["REPORT_DIR"], fresh_log_info.date)
    logging.info('work finished, result in %s' % merged_config["REPORT_DIR"])


if __name__ == "__main__":

    args = parse_args()

    config_from_file_path = args.config
    bad_parse_thresh = args.bad_parse_thresh
    logging_file_path = args.logging_file_path

    setup_logger(logging_file_path)

    merged_config = config
    if config_from_file_path:
        try:
            merged_config = merge_config(config, config_from_file_path)
        except IOError:
            sys.exit(1)

    try:
        main()
    except SystemExit:
        raise
    except BaseException as e:
        logging.exception('Something went wrong. Details: %s' % e.message)
    

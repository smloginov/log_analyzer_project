#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import unittest
import shutil
import tempfile
import gzip
import bz2
from log_analyzer.log_analyzer import (
    merge_config,
    config,
    find_log_file
)


class TestMergeConfig(unittest.TestCase):
    def test_non_existing_config(self):
        with self.assertRaises(OSError):
            merge_config(config,
                         os.path.join('tests', 'fixtures',
                                      'not_exist_config.json'))

    def test_parse_error_config(self):
        with self.assertRaises(ValueError):
            merge_config(config,
                         os.path.join('tests', 'fixtures',
                                      'incorrect_config.json'))

    def test_empty_config(self):
        merged_config = merge_config(config,
                                     os.path.join('tests', 'fixtures',
                                                  'empty_config.json'))
        self.assertDictEqual(merged_config, config)


class TestFindLogFile(unittest.TestCase):
    def setUp(self):
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_dir)

    def test_gzip_log(self):
        logs_dict = {'bz2_ext': 'nginx-access-ui.log-20190106.bz2',
                     'gz_ext': 'nginx-access-ui.log-20190108.gz'}

        with gzip.open(os.path.join(self.test_dir,
                                    logs_dict['gz_ext']), 'w') as f_gz:
            f_gz.write(b'empty string')

        f_bz2 = bz2.BZ2File(os.path.join(self.test_dir,
                                         logs_dict['bz2_ext']), 'w')
        f_bz2.write(b'empty string')
        f_bz2.close()

        self.assertEqual(logs_dict['gz_ext'],
                         find_log_file(self.test_dir).name)

    def test_other_nginx_service_log(self):
        logs_list = ['nginx-access-ui.log-20190108',
                     'nginx-access-other_service.log-20190108']

        for fn in logs_list:
            with open(os.path.join(self.test_dir, fn), 'w') as f:
                f.write('test string')

        self.assertEqual('nginx-access-ui.log-20190108',
                         find_log_file(self.test_dir).name)

    def test_the_most_fresh_log(self):
        logs_list = ['nginx-access-ui.log-20190108',
                     'nginx-access-ui.log-20180108',
                     'nginx-access-ui.log-20170108']

        for fn in logs_list:
            with open(os.path.join(self.test_dir, fn), 'w') as f:
                f.write('test string')

        self.assertEqual('nginx-access-ui.log-20190108',
                         find_log_file(self.test_dir).name)

    def test_no_fresh_log(self):
        logs_list = ['nginx-access-one_service.log-20190108',
                     'nginx-access-two_service.log-20180108',
                     'other_log_stuff.log']

        for fn in logs_list:
            with open(os.path.join(self.test_dir, fn), 'w') as f:
                f.write('test string')

        self.assertEqual('',
                         find_log_file(self.test_dir).name)


if __name__ == "__main__":
    unittest.main()

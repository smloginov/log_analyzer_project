from setuptools import setup

setup(
    name='log_analyzer',
    version='0.0.1',
    description='command line tool for analyzing nginx access logs',
    license='MIT',
    url='https://gitlab.com/smloginov/log_analyzer_project',
    packages=['log_analyzer'],
    entry_points={
        'console_scripts': [
            'log_analyzer = log_analyzer.log_analyzer:main',
        ],
    },
)
